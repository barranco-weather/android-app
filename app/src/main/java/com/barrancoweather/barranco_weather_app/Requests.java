package com.barrancoweather.barranco_weather_app;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

// A class to manage all the request necessary for the App
public class Requests {
    public Context mContext;
    private static final String TAG = "Requests";
    public Requests(Context context) {
        mContext = context;
    }

    // Interfaces provide a way to keep coding with data from this class in another context
    public interface VolleyCallback {
        void onSuccess(String resp);
        void onFailure(String error);
    }

    public void getLastMeasure(final String apiKey, final VolleyCallback callback) {
        // Here goes the url of your endpoint
        String url = "http://www.innovations.pe/arturo/api-bweather/measures/read_last/";
        // Remember to specify the Method you will use
        int  requestMethod = Request.Method.POST;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(requestMethod, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccess(response.toString());
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + apiKey);
                callback.onFailure(error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("User-Agent","android");
                headers.put("Content-Type","application/json");
                return headers;
            }

//            // This is for POST requests

            @Override
            public byte[] getBody() {
                HashMap<String, String> params = new HashMap<>();
                params.put("api_key",apiKey);
                return new JSONObject(params).toString().getBytes();
            }
        };

        // After everything is ready, add the request to the Volley's Request Queue
        Singleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }
}

