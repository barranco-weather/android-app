package com.barrancoweather.barranco_weather_app;

import com.android.volley.RequestQueue;
import android.content.Context;

import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
/*
Quote from Wikipedia about singletons:
"In software engineering, the singleton pattern is a software design pattern
that restricts the instantiation of a class to one object. This is useful
when exactly one object is needed to coordinate actions across the system."

In this case, we use a singleton because we want to create only one way to
access Volley's Request Queue, which decides which request has priority
among the others.

It's safe to copy and paste this class exactly as it is.
*/
public class Singleton {
    private static Singleton mInstance;
    private RequestQueue mRequestQueue;
    private static Context mContext;

    private Singleton(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    public static synchronized Singleton getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new Singleton(context);
        }
        return mInstance;
    }

    public <T> void addToRequestQueue(JsonObjectRequest req) {
        getRequestQueue().add(req);
    }
}

