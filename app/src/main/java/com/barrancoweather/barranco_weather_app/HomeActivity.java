package com.barrancoweather.barranco_weather_app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Locale;

public class HomeActivity extends AppCompatActivity {
    RelativeLayout mainContainer;
    TextView temp, hum, uv, bmp, ultimaMedicion, uvStatus, recomendaciones;
    private static final String TAG = "HomeActivity";
    Requests rq = new Requests(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        temp = findViewById(R.id.home_temperature);
        hum = findViewById(R.id.home_humedad);
        uv = findViewById(R.id.home_uv);
        bmp = findViewById(R.id.home_presion);
        uvStatus = findViewById(R.id.home_uv_status);
        ultimaMedicion = findViewById(R.id.home_ultima_medicion);
        recomendaciones = findViewById(R.id.home_recomendaciones);

        final String apiKey = "Uk1Ab2nvLvskmod";

        rq.getLastMeasure(apiKey, new Requests.VolleyCallback() {
            @Override
            public void onSuccess(String resp) {
                try {
                    Log.d(TAG, "onSuccess: " + resp);
                    JSONObject measure = new JSONObject(resp);

                    Double temperatura = measure.getDouble("temperature");
                    Double rad_uv = measure.getDouble("uv");
                    Double presion = measure.getDouble("bmp");
                    Double humedad = measure.getDouble("rel_humidity");
                    String uv_status = measure.getString("uv_status");

                    String temperatureTxt = String.valueOf(temperatura) + "°C";
                    String humTxt = String.valueOf(humedad) + "R/H";
                    DecimalFormat df = new DecimalFormat("#.##");
                    String uvTxt = df.format(rad_uv) + " nm";
                    String bmpTxt = df.format(presion) + " atm";
                    temp.setText(temperatureTxt);
                    hum.setText(humTxt);
                    uv.setText(uvTxt);
                    bmp.setText(bmpTxt);
                    uvStatus.setText(uv_status);

                    String[] time = measure.getString("created").split("\\s+");
                    String actualTime = time[1];
                    DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                    try {
                        java.sql.Time timeValue = new java.sql.Time(formatter.parse(actualTime).getTime() - 60 * 60 * 5000);
                        String ultimaMedicionTxt = "Última medición tomada a las " + String.valueOf(timeValue);
                        ultimaMedicion.setText(ultimaMedicionTxt);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    String recoms = "";

                    if (temperatura > 0 && temperatura < 20) {
                        recoms += "Hace frío. Abrigate y toma precauciones para no enfermarte\n\n";
                    } else if (temperatura > 30) {
                        recoms += "Empieza a hacer calor. Procura llevar ropa ligera\n\n";
                    } else {
                        recoms += "Hay una temperatura agradable. Pero cuidado, en Barranco la temperatura cambia rápido. No te confíes\n\n";
                    }

                    switch (uv_status) {
                        case "Bajo": recoms += "La radiación solar es baja. No es necesaria protección adicional. ¡Pero no mires directamente al sol!\n\n";break;
                        case "Moderado": recoms += "Deberías utilizar bloqueador solar si planeas estar mucho tiempo al sol.\n\n";break;
                        case "Alto": recoms += "Es necesario que uses protección UV ahora.\n\n";break;
                        case "Muy Alto": recoms += "Busca lugares con sombra, aplicate bloqueador solar y reaplicalo constantemente\n\n";break;
                        case "Extremo": recoms += "La radiación solar está en niveles postapocalipticos. Busca cobertura bajo tierra\n\n";break;
                    }

                    if(presion >= 1.5) recoms += "Estas a una altura considerable. Si te da soroche, mastica hojas de coca y no continues haciendo actividades físicas hasta que mejores\n\n";

//                    String test = recoms + recoms + recoms +recoms+recoms;
//                    recomendaciones.setText(test);
                    recomendaciones.setText(recoms);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Log.d(TAG, "onFailure: " + error);
                Toast.makeText(HomeActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
